#!/bin/env bash

for file in $1/_*
do
    fileName=$(basename $file)
    dirName=$(dirname $file)
    convert "$file" \( +clone -geometry x2160 \) \( -clone 0 -geometry 3840x -gravity center -crop 3840x2160+0+0 -blur 0x9 \) \
        -delete 0 +swap -gravity center -compose over -composite $dirName/${fileName:1}
done
